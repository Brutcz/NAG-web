$(document).ready(function(){
	var serverOut = [];
	var quantity,title;

  switch((document.URL).split("6666/")[1]){ //Na základě url zvolí veličinu
    case 'teplota': quantity = "temp"; title = "Teplota"; break;
    case 'vlhkost': quantity = "hum"; title = "Vlhkost"; break;
    case 'svetlo': quantity = "lux"; title = "Světelnost"; break;
  }

  //vytáhne data
  function getData(){
    		$.ajax({
            url: "./getData.php",
            type: "POST",
            async: false,
            dataType: "json"
	        }).done(function(data){

	            if(data[0]==true){
	                data.shift();
                  data.reverse();  //Převratí pole s daty
	                
	                serverOut = data;
	            }
	            else {
	                console.log("fail"); //pouze kontrola funkčnosti
	                console.log("Někde se stala chyba!");
	            }
	        });
  }

  google.setOnLoadCallback(drawChart);

  function getJsonTime(id){ //Zpracování formátu data
  	var t = JSON.parse(serverOut[id])["time"].split(/[- :]/);
		var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
		
		var min = d.getMinutes();
		if(min<10) min = "0"+min;
		var time = d.getDate()+"."+(d.getMonth()+1)+" "+d.getHours()+":"+min;

		return time;
  }

  //vykreslení grafu
  function drawChart() {
  	var data2 = getData();

    //data pro graf
    var data = google.visualization.arrayToDataTable([
      ['Hodina', title ],
      [getJsonTime(0),  JSON.parse(serverOut[0])[quantity]],
      [getJsonTime(1),  JSON.parse(serverOut[1])[quantity]],
      [getJsonTime(2),  JSON.parse(serverOut[2])[quantity]],
      [getJsonTime(3),  JSON.parse(serverOut[3])[quantity]],
      [getJsonTime(4),  JSON.parse(serverOut[4])[quantity]],
      [getJsonTime(5),  JSON.parse(serverOut[5])[quantity]]
    ]);

    //nastavení google grafu
    var options = {
      title: title,
      curveType: 'function',
      legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

    chart.draw(data, options);

    //Zobrazování obrázku a den/noc
    if(quantity == "lux"){
      if(JSON.parse(serverOut[5])["lux"]>200){
        $("#doba").html("Den");
        $("#timeImg").prop("src","./Obrazky/slunce.PNG");
      } else {
        $("#doba").html("Noc");
        $("#timeImg").prop("src","./Obrazky/moon.png");
      }
    }
  }
});