// scripty by Martin Fabík 
var maxBgTop = -120;
var initialBgTop = 0;

$( window ).scroll(function() {
	$(".bg-cover").css("background-position-y",(document.body.scrollTop/((($(".bg-cover").height()/1.2)-(document.body.offsetHeight))/(initialBgTop-maxBgTop))+initialBgTop));
});

$( window ).load(function() {
	$(".bg-cover").css("background-position-y",initialBgTop);
	$(".bg-cover").css("background-position-x",-($(".bg-cover").width()-$("body").width())/2);
});

$( window ).resize(function() {
	$(".bg-cover").css("background-position-x",-($(".bg-cover").width()-$("body").width())/2);
});