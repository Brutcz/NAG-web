$(document).ready(function(){
	var serverOut = [];
	function getData(){
    		$.ajax({
            url: "./getData.php",
            type: "POST",
            //data: ajaxData,
            async: false,
            dataType: "json"
	        }).done(function(data){
	            //console.log(data); //ověření dat

	            if(data[0]==true){
	                console.log("succes"); //pouze kontrola funkčnosti
	                console.log(data);
	                //console.log(JSON.parse(data));
	                data.reverse();
	                
	                serverOut = data;
	            }
	            else {
	                console.log("fail"); //pouze kontrola funkčnosti
	                window.alert("Někde se stala chyba!");

	            }
	        });
    	}

    	getData();

      google.setOnLoadCallback(drawChart);

      function getJsonTime(id){
      	var t = JSON.parse(serverOut[id])["time"].split(/[- :]/);
		var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
		//console.log(d);
		var min = d.getMinutes();
		if(min<10) min = "0"+min;
		var time = d.getDate()+"."+(d.getMonth()+1)+" "+d.getHours()+":"+min;
		return time;
      }

      function drawChart() {
      	var data2 = getData();
      	//console.log(JSON.parse(serverOut[0])["temp"]);
        var data = google.visualization.arrayToDataTable([
          ['Hodina', 'Vlhkost' ],
          [getJsonTime(0),  JSON.parse(serverOut[0])["hum"]],
          [getJsonTime(1),  JSON.parse(serverOut[1])["hum"]],
          [getJsonTime(2),  JSON.parse(serverOut[2])["hum"]],
          [getJsonTime(3),  JSON.parse(serverOut[3])["hum"]],
          [getJsonTime(4),  JSON.parse(serverOut[4])["hum"]],
          [getJsonTime(5),  JSON.parse(serverOut[5])["hum"]]
        ]);

        var options = {
          title: 'Vlhkost',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
});