<?php
	include('db_conn.php');

	$stmt=$mysqli->prepare('SELECT * FROM sensorData ORDER BY id_rec DESC LIMIT 1');
	$stmt->execute();
	$stmt->bind_result($ID,$temp,$hum,$lux,$time);
	$stmt->fetch();
	$stmt->close();

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<meta name="description" content="Návrh na stránku grafu"/>
		<meta name="author" content=""/>
		<link rel="stylesheet" href="./CSS/styles.css">
		<!--link rel="stylesheet" href="./CSS/bootstrap.min.css"-->
		<link rel="stylesheet" href="./CSS/bootstrap-paper.min.css">
		<link rel="stylesheet" href="./CSS/font-awesome.min.css">
		<script src="./JS/jquery-2.1.0.min.js"></script>
		<script src="./JS/bootstrap.min.js"></script>
		<title>Tým horkých malin</title>
	</head>
	<body class="body">
		<?php include('nav.php');?>

		<div class="jumbotron">
			<div class="container">
				<div id="graphs">
					<div class="page-header">
						<h1>Výpis hodnot z našeho RPi</h1>
					</div>
					<div class="row">
					  <div class="col-sm-6 col-md-4">
					    <div class="thumbnail">
					      <div class="caption">
					        <h3>Teplota</h3>
					        <p>Aktuální teplota</p>
						<p><?php echo $temp." °C";?></p>
					        <p>
					        	<a href="teplota" class="btn btn-primary" role="button">Detaily</a>
					        </p>
					      </div>
					    </div>
					  </div>
					  <div class="col-sm-6 col-md-4">
					    <div class="thumbnail">
					      <div class="caption">
					        <h3>Vlhkost</h3>
					        <p>Aktuální vlhkost</p>
					        <p><?php echo $hum." %";?></p>
						<p>
					        	<a href="vlhkost" class="btn btn-primary" role="button">Detaily</a>
					        </p>
					      </div>
					    </div>
					  </div>
					  <div class="col-sm-6 col-md-4">
					    <div class="thumbnail">
					      <div class="caption">
					        <h3>Světelnost</h3>
					        <p>Aktuální intenzita světla</p>
						<p><?php echo round($lux,4)." Lx";?></p>
					        <p>
					        	<a href="svetlo" class="btn btn-primary" role="button">Detaily</a>
					        </p>
					      </div>
					    </div>
					  </div>
					</div>
				<?php echo "Čas: ".$time;?>
				</div>
				<div id="AboutUs">
					<div class="page-header">
					   <div class="row">
					  <div class="col-sm-6 col-md-12">
					    	<div class="thumbnail">
					      		<div class="caption"> 
									<h2>O nás</h2>
									<p>Pár slov o nás...<a class="btn btn-primary" href="./onas" style="margin-left:15px;">Najdete zde</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p>&copy; Horké Maliny</p>
		</footer>
	</body>
</html>
