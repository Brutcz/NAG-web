<?php
	include('db_conn.php');

	header('Content-Type: application/json ; charset=utf-8');

	$data = array();

	$stmt=$mysqli->prepare('SELECT * FROM `sensorData` WHERE `id_rec`%10 = 0 ORDER BY `id_rec` DESC LIMIT 6'); //SELECT * FROM sensorData ORDER BY id_rec DESC LIMIT 60
	$stmt->execute();
	$stmt->bind_result($ID,$temp,$hum,$lux,$time);
	$stmt->store_result();
	$n = $stmt->num_rows;
	$data[0] = ($n>0)? true:false;
	while($row = $stmt->fetch())
	{
		$ID = intval(str_replace("L", "", $ID));
		array_push($data, '{"id":'.$ID.',"temp":'.$temp.',"hum":'.$hum.',"lux":'.$lux.',"time":"'.$time.'"}');
	}
	$stmt->close();

	echo json_encode($data);


