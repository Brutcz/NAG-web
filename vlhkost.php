<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<meta name="description" content="Návrh na stránku grafu"/>
		<meta name="author" content=""/>
		<link rel="stylesheet" href="./CSS/styles.css">
		<!--link rel="stylesheet" href="./CSS/bootstrap.min.css"-->
		<link rel="stylesheet" href="./CSS/bootstrap-paper.min.css">
		<link rel="stylesheet" href="./CSS/font-awesome.min.css">
		<script src="./JS/jquery-2.1.0.min.js"></script>
		<script src="./JS/bootstrap.min.js"></script>
		<title>Graf vlhkosti</title>
		<script type="text/javascript"
         	src="https://www.google.com/jsapi?autoload={
            		'modules':[{
            		'name':'visualization',
            		'version':'1',
            		'packages':['corechart']
            	}]
          	}">
        </script>

    	<script type="text/javascript" src="./JS/main.js"></script>
	</head>
	<body class="body">
		<?php include('nav.php');?>

		<div class="jumbotron">
			<div class="container">
				<div id="graphs">
					<div class="page-header">
						<h1>Výpis hodnot z našeho RPi</h1>
					</div>
						<div class="row">
					  		<div class="col-sm-6 col-md-12">
					    		<div class="thumbnail">
					     			 <div class="caption">
					        <h3>Vlhkost</h3>
					        <p>Graf vlhkosti</p>
					        <div id="curve_chart" style="width: 1100px; height: 400px"></div>
					      </div>
					    </div>
					  </div>
					  
					  </div>
					</div>
				</div>
				
			</div>
		</div>
		<footer>
			<p>&copy; Horké Maliny</p>
		</footer>
	</body>
</html>
