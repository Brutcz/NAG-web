<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<meta name="description" content="Návrh na stránku grafu"/>
		<meta name="author" content=""/>
		<link rel="stylesheet" href="./CSS/styles.css">
		<!--link rel="stylesheet" href="./CSS/bootstrap.min.css"-->
		<link rel="stylesheet" href="./CSS/bootstrap-paper.min.css">
		<link rel="stylesheet" href="./CSS/font-awesome.min.css">
		<script src="./JS/jquery-2.1.0.min.js"></script>
		<script src="./JS/bootstrap.min.js"></script>
		<title>O nás</title>
	</head>
	<body class="body">
		<?php include('nav.php');?>

		<div class="jumbotron">
			<div class="container">

				<div id="AboutUs">
					<div class="page-header">
					   <div class="row">
					  <div class="col-sm-6 col-md-12">
					    	<div class="thumbnail">
					      		<div class="caption"> 
									<h2>O nás</h2>
									<p>Pár slov o nás...</p>
									<!--p>Neumíme vůbec nic :D (říkají nám to pořád, ale my se za to nestydíme).</p-->
									<p>Jsme tým 4 lidí(+ náš konzultant):</p>
									<ul>
										<li>
											<h3>Náš team führer</h3>
											<p>Ing.Jiří Kačmařík</p>
										</li>
										<li>
											<h3>Programátor</h3>
											<p>Filip Kaňák</p>
										</li>
										<li>
											<h3>Programátor, síťař</h3>
											<p>Michal Byrtus</p>
										</li>
										<li>
											<h3>Elektrotechnik</h3>
											<p>Richard Košťál</p>
										</li>
										<li>
											<h3>Konzultant</h3>
											<p>Martin Fabík</p>
										</li>
									</ul>
									<p>Mnohdy to i bývá, tak že všichni dělají od každého trochu.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p>&copy; Horké Maliny</p>
		</footer>
	</body>
</html>
