<?php
	$host="localhost"; // Host name 
	$db_username= getenv("DB_USER"); // Mysql username 
	$db_password= getenv("DB_PASS"); // Mysql password 
	$db_name= getenv("DB_DB"); // Database name 

	// Connect to server and select databse.
	$mysqli = new mysqli($host, $db_username, $db_password, $db_name);
	if ($mysqli->connect_errno) {
    	echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	//echo $mysqli->host_info . "\n";  //Kontrola připojení

	$mysqli->query("SET CHARACTER SET utf8");

